import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { User } from './users/entities/user.entity';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClassesModule } from './classes/classes.module';
import { Class } from './classes/entities/class.entity';
import { StatusModule } from './status/status.module';
import { Status } from './status/entities/status.entity';
import { MembersModule } from './members/members.module';
import { Member } from './members/entities/member.entity';
import { TrainersModule } from './trainers/trainers.module';
import { Trainer } from './trainers/entities/trainer.entity';
import { AccountsModule } from './accounts/accounts.module';
import { Account } from './accounts/entities/account.entity';
import { ExpanseModule } from './expanses/expanse.module';
import { Expanse } from './expanses/entities/expanse.entity';
import { PtsModule } from './pts/pts.module';
import { Pt } from './pts/entities/pt.entity';


@Module({
  imports: [
    TypeOrmModule.forRoot(
      {
        type: 'sqlite',
        database: 'db.sqlite',
        entities: [Class, Status, Member, Trainer, Account, Expanse, Pt,],
        migrations: [],
        synchronize: true,
      },
      // {
      //   type: 'mysql',
      //   host: 'sql6.freesqldatabase.com',
      //   port: 3306,
      //   username: 'sql6683786',
      //   password: 'Wd6DynSbcH',
      //   database: 'sql6683786',
      //   entities: [User, Class],

      //   synchronize: true,
      // }
    ),
    //UsersModule,
    ClassesModule,
    StatusModule,
    MembersModule,
    TrainersModule,
    AccountsModule,
    ExpanseModule,
    PtsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
