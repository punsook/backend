import { IsNotEmpty, Length } from 'class-validator';

export class CreateMemberDto {
  @IsNotEmpty()
  member_name: string;

  member_gender: string;

  member_dob: Date;

  @IsNotEmpty()
  member_age: number;

  @IsNotEmpty()
  @Length(10)
  member_phone: string;

  @IsNotEmpty()
  member_email: string;

  @IsNotEmpty()
  member_address: string;

  member_start_date: Date;
}
