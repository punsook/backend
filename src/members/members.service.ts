import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { Member } from './entities/member.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MembersService {
  constructor(
    @InjectRepository(Member)
    private membersRepository: Repository<Member>,
  ) {}
  findAll() {
    return this.membersRepository.find();
  }
  async findOne(id: number) {
    const member = await this.membersRepository.findOne({
      where: { member_id: id },
    });
    if (!member) {
      throw new NotFoundException();
    }
    return member;
  }
  create(createTrainerDto: CreateMemberDto) {
    const member = this.membersRepository.save(createTrainerDto);
    if (!member) {
      throw new NotFoundException();
    }
    return member;
  }
  async update(id: number, updateMembermerDto: UpdateMemberDto) {
    const member = await this.membersRepository.findOneBy({ member_id: id });
    if (!member) {
      throw new NotFoundException();
    }
    const updatedMember = { ...member, ...updateMembermerDto };
    return this.membersRepository.save(updatedMember);
  }
  async remove(id: number) {
    const member = await this.membersRepository.findOneBy({ member_id: id });
    if (!member) {
      throw new NotFoundException();
    }
    return this.membersRepository.softRemove(member);
  }
}
