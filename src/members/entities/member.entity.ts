import { Account } from 'src/accounts/entities/account.entity';
import { Pt } from 'src/pts/entities/pt.entity';
import { Status } from 'src/status/entities/status.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Member {
  @PrimaryGeneratedColumn()
  member_id: number;

  @Column()
  member_name: string;

  @Column()
  member_gender: string;

  @Column()
  member_dob: Date;

  @Column()
  member_age: number;

  @Column()
  member_phone: string;

  @Column()
  member_email: string;

  @Column()
  member_address: string;

  @Column()
  member_start_date: Date;

  @OneToOne(() => Account, (account) => account.member)
  @JoinColumn()
  account: Account[];

  @OneToMany(() => Pt, (pt) => pt.member)
  pt: Pt[];
}
