import { IsNotEmpty, Length, Matches } from 'class-validator';

export class CreateAccountDto {
  @IsNotEmpty()
  @Length(10)
  account_phone: string;

  @IsNotEmpty()
  @Matches(
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
  )
  account_password: string;

  @IsNotEmpty()
  account_role: string;
}
