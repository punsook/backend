import { Expanse } from 'src/expanses/entities/expanse.entity';
import { Member } from 'src/members/entities/member.entity';
import { Trainer } from 'src/trainers/entities/trainer.entity';
import {
  Column,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Account {
  @PrimaryGeneratedColumn()
  account_id: number;

  @Column()
  account_phone: string;

  @Column()
  account_password: string;

  @Column()
  account_role: string;

  @DeleteDateColumn()
  del: Date;

  @OneToMany(() => Expanse, (expanse) => expanse.account)
  expanse: Expanse[];

  @OneToOne(() => Member, (member) => member.account)
  @JoinColumn()
  member: Member[];

  @OneToOne(() => Trainer, (trainer) => trainer.account)
  @JoinColumn()
  trainer: Trainer[];
}
