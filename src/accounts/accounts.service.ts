import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAccountDto } from './dto/create-account.dto';
import { UpdateAccountDto } from './dto/update-account.dto';
import { Account } from './entities/account.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AccountsService {
  constructor(
    @InjectRepository(Account)
    private accountsRepository: Repository<Account>,
  ) {}
  create(createAccountDto: CreateAccountDto) {
    const account = this.accountsRepository.save(createAccountDto);
    if (!account) {
      throw new NotFoundException();
    }
    return account;
  }

  findAll() {
    return this.accountsRepository.find({
      relations: ['member', 'trainer'],
    });
  }

  async findOne(id: number) {
    const account = await this.accountsRepository.findOne({
      where: { account_id: id },
    });
    if (!account) {
      throw new NotFoundException();
    }
    return account;
  }

  async update(id: number, updateAccountDto: UpdateAccountDto) {
    const account = await this.accountsRepository.findOneBy({ account_id: id });
    if (!account) {
      throw new NotFoundException();
    }
    const updatedaccount = { ...account, ...updateAccountDto };
    return this.accountsRepository.save(updatedaccount);
  }

  async remove(id: number) {
    const account = await this.accountsRepository.findOneBy({ account_id: id });
    if (!account) {
      throw new NotFoundException();
    }
    return this.accountsRepository.softRemove(account);
  }
}
