import { Class } from 'src/classes/entities/class.entity';
import { Member } from 'src/members/entities/member.entity';
import { Trainer } from 'src/trainers/entities/trainer.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Timestamp,
} from 'typeorm';


@Entity()
export class Status {
  [x: string]: any;
  @PrimaryGeneratedColumn()
  status_id: number;

  @Column({default: 'Success'})
  status_name: string;

  // @Column()
  // date: Timestamp;

  @CreateDateColumn()
  status_booking_time: Date; 


  @ManyToOne(() => Class, (classes) => classes.status)
  classes: Class[];

}
