import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateStatusDto } from './dto/create-status.dto';
import { UpdateStatusDto } from './dto/update-status.dto';
import { Status } from './entities/status.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StatusService {
  constructor(
    @InjectRepository(Status)
    private statusRepository: Repository<Status>,
  ) {}
  create(createStatusDto: CreateStatusDto) {
    const status = this.statusRepository.save(createStatusDto);
    if (!status) {
      throw new NotFoundException();
    }
    return status;
  }

  findAll() {
    return this.statusRepository.find({
      relations: ['classes'],
    });
  }

  findOne(id: number) {
    return this.statusRepository.findOneBy({ status_id: id });
  }

  async update(id: number, updateStatusDto: UpdateStatusDto) {
    const status = await this.statusRepository.findOneBy({ status_id: id });
    const updatedStatus = { ...status, ...updateStatusDto };
    return this.statusRepository.save(updatedStatus);
  }

  async remove(id: number) {
    const status = await this.statusRepository.findOneBy({ status_id: id });
    return this.statusRepository.remove(status);
  }
}
