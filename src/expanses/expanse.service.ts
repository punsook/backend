import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateExpanseDto } from './dto/create-expanse.dto';
import { UpdateExpanseDto } from './dto/update-expanse.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Expanse } from './entities/expanse.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ExpanseService {
  constructor(
    @InjectRepository(Expanse)
    private expansesRepository: Repository<Expanse>,
  ) {}
  create(createExpanseDto: CreateExpanseDto) {
    const expanse = this.expansesRepository.save(createExpanseDto);
    if (!expanse) {
      throw new NotFoundException();
    }
    return expanse;
  }

  findAll() {
    return this.expansesRepository.find({
      relations: ['account'],
    });
  }

  async findOne(id: number) {
    const expanse = await this.expansesRepository.findOne({
      where: { expanse_id: id },
    });
    if (!expanse) {
      throw new NotFoundException();
    }
    return expanse;
  }

  async update(id: number, updateExpanseDto: UpdateExpanseDto) {
    const expanse = await this.expansesRepository.findOneBy({ expanse_id: id });
    if (!expanse) {
      throw new NotFoundException();
    }
    const updatedexpanse = { ...expanse, ...updateExpanseDto };
    return this.expansesRepository.save(updatedexpanse);
  }

  async remove(id: number) {
    const expanse = await this.expansesRepository.findOneBy({ expanse_id: id });
    if (!expanse) {
      throw new NotFoundException();
    }
    return this.expansesRepository.softRemove(expanse);
  }
}
