import { Module } from '@nestjs/common';
import { ExpanseService } from './expanse.service';
import { ExpanseController } from './expanse.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Expanse } from './entities/expanse.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Expanse])],
  controllers: [ExpanseController],
  providers: [ExpanseService],
  exports: [ExpanseService],
})
export class ExpanseModule {}
