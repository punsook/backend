import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ExpanseService } from './expanse.service';
import { CreateExpanseDto } from './dto/create-expanse.dto';
import { UpdateExpanseDto } from './dto/update-expanse.dto';

@Controller('expanse')
export class ExpanseController {
  constructor(private readonly expanseService: ExpanseService) {}

  @Post()
  create(@Body() createExpanseDto: CreateExpanseDto) {
    return this.expanseService.create(createExpanseDto);
  }

  @Get()
  findAll() {
    return this.expanseService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.expanseService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateExpanseDto: UpdateExpanseDto) {
    return this.expanseService.update(+id, updateExpanseDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.expanseService.remove(+id);
  }
}
