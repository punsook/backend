import { Test, TestingModule } from '@nestjs/testing';
import { ExpanseService } from './expanse.service';

describe('ExpanseService', () => {
  let service: ExpanseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExpanseService],
    }).compile();

    service = module.get<ExpanseService>(ExpanseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
