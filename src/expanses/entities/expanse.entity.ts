import { Account } from 'src/accounts/entities/account.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Expanse {
  @PrimaryGeneratedColumn()
  expanse_id: number;

  @Column()
  expanse_type: string;

  @Column()
  expanse_name: string;

  @CreateDateColumn()
  expanse_date: Date;

  @Column()
  expanse_price: number;

  @DeleteDateColumn()
  delDate: Date;

  @ManyToOne(() => Account, (account) => account.expanse)
  account: Account;
}
