import { Test, TestingModule } from '@nestjs/testing';
import { ExpanseController } from './expanse.controller';
import { ExpanseService } from './expanse.service';

describe('ExpanseController', () => {
  let controller: ExpanseController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExpanseController],
      providers: [ExpanseService],
    }).compile();

    controller = module.get<ExpanseController>(ExpanseController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
