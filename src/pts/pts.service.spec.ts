import { Test, TestingModule } from '@nestjs/testing';
import { PtsService } from './pts.service';

describe('PtsService', () => {
  let service: PtsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PtsService],
    }).compile();

    service = module.get<PtsService>(PtsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
