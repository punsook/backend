import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePtDto } from './dto/create-pt.dto';
import { UpdatePtDto } from './dto/update-pt.dto';
import { Pt } from './entities/pt.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class PtsService {
  constructor(
    @InjectRepository(Pt)
    private ptsRepository: Repository<Pt>,
  ) {}
  create(createPtDto: CreatePtDto) {
    const pt = this.ptsRepository.save(createPtDto);
    if (!pt) {
      throw new NotFoundException();
    }
    return pt;
  }

  findAll() {
    return this.ptsRepository.find({
      relations: ['member'],
    });
  }

  async findOne(id: number) {
    const pt = await this.ptsRepository.findOne({
      where: { pt_id: id },
    });
    if (!pt) {
      throw new NotFoundException();
    }
    return pt;
  }

  async update(id: number, updatePtDto: UpdatePtDto) {
    const pt = await this.ptsRepository.findOneBy({ pt_id: id });
    if (!pt) {
      throw new NotFoundException();
    }
    const updatedpt = { ...pt, ...updatePtDto };
    return this.ptsRepository.save(updatedpt);
  }

  async remove(id: number) {
    const pt = await this.ptsRepository.findOneBy({ pt_id: id });
    if (!pt) {
      throw new NotFoundException();
    }
    return this.ptsRepository.softRemove(pt);
  }
}
