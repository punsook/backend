import { Min } from 'class-validator';

export class CreatePtDto {
  @Min(0)
  pt_duration: number;
}
