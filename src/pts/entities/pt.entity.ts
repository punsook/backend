import { Member } from 'src/members/entities/member.entity';
import { Trainer } from 'src/trainers/entities/trainer.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Pt {
  @PrimaryGeneratedColumn()
  pt_id: number;

  @Column({ default: 'Success' })
  pt_status: string;

  @Column({ default: 'PT Session' })
  pt_name: string;

  @Column()
  pt_duration: number;

  @Column()
  pt_trainer: string;

  @CreateDateColumn()
  pt_createdAt: Date;

  @DeleteDateColumn()
  delDate: Date;

  @ManyToOne(() => Member, (member) => member.pt)
  member: Member;
}
