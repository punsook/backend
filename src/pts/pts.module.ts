import { Module } from '@nestjs/common';
import { PtsService } from './pts.service';
import { PtsController } from './pts.controller';
import { Pt } from './entities/pt.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Pt])],
  controllers: [PtsController],
  providers: [PtsService],
  exports: [PtsService],
})
export class PtsModule {}
