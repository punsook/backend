import { Test, TestingModule } from '@nestjs/testing';
import { PtsController } from './pts.controller';
import { PtsService } from './pts.service';

describe('PtsController', () => {
  let controller: PtsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PtsController],
      providers: [PtsService],
    }).compile();

    controller = module.get<PtsController>(PtsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
