import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTrainerDto } from './dto/create-trainer.dto';
import { UpdateTrainerDto } from './dto/update-trainer.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Trainer } from './entities/trainer.entity';

@Injectable()
export class TrainersService {
  constructor(
    @InjectRepository(Trainer)
    private trainersRepository: Repository<Trainer>,
  ) {}
  findAll() {
    return this.trainersRepository.find();
  }
  async findOne(id: number) {
    const trainer = await this.trainersRepository.findOne({
      where: { trainer_id: id },
    });
    if (!trainer) {
      throw new NotFoundException();
    }
    return trainer;
  }
  create(createTrainerDto: CreateTrainerDto) {
    const trainer = this.trainersRepository.save(createTrainerDto);
    if (!trainer) {
      throw new NotFoundException();
    }
    return trainer;
  }
  async update(id: number, updateTrainerDto: UpdateTrainerDto) {
    const trainer = await this.trainersRepository.findOneBy({ trainer_id: id });
    if (!trainer) {
      throw new NotFoundException();
    }
    const updatedTrainer = { ...trainer, ...updateTrainerDto };
    return this.trainersRepository.save(updatedTrainer);
  }
  async remove(id: number) {
    const trainer = await this.trainersRepository.findOneBy({ trainer_id: id });
    if (!trainer) {
      throw new NotFoundException();
    }
    return this.trainersRepository.softRemove(trainer);
  }
}
