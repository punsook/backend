import { Account } from 'src/accounts/entities/account.entity';
import { Class } from 'src/classes/entities/class.entity';
import { Pt } from 'src/pts/entities/pt.entity';
import { Status } from 'src/status/entities/status.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  DeleteDateColumn,
  OneToMany,
  OneToOne,
  JoinColumn,
  ManyToMany,
  JoinTable,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Trainer {
  @PrimaryGeneratedColumn()
  trainer_id: number;

  @Column()
  trainer_name: string;

  @Column()
  trainer_gender: string;

  @Column()
  trainer_dob: string;

  @Column()
  trainer_age: number;

  @Column()
  trainer_phone: string;

  @Column()
  trainer_email: string;

  @Column()
  trainer_address: string;

  @Column()
  trainer_facebook: string;

  @Column()
  trainer_start_date: string;

  @Column()
  trainer_style: string;

  @DeleteDateColumn()
  del: Date;

  @OneToOne(() => Account, (account) => account.trainer)
  @JoinColumn()
  account: Account[];
}
