import { Status } from 'src/status/entities/status.entity';
import { Trainer } from 'src/trainers/entities/trainer.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Class {
  @PrimaryGeneratedColumn()
  classes_id: number;

  @Column()
  classes_name: string;

  @Column()
  classes_time: string;

  @Column()
  classes_room: number;

  @Column({ default: 'Class Room' })
  classes_activity: string;

  @Column({ default: 0 })
  classes_qty: number;

  @Column()
  classes_trainer: string;

  // @Column({ default: 'noimg.jpg' })
  // classes_img: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => Status, (status) => status.classes)
  status: Status[];
}
