import { IsNotEmpty, Length } from 'class-validator';

export class CreateClassDto {
  @IsNotEmpty()
  @Length(3, 32)
  classes_name: string;

  classes_time: string;

  classes_room: number;

  classes_trainer: string;

  classes_activity: string;

  classes_qty: number;
}
