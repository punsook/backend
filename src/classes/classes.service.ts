import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateClassDto } from './dto/create-class.dto';
import { UpdateClassDto } from './dto/update-class.dto';
import { Class } from './entities/class.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { error, log } from 'console';

@Injectable()
export class ClassesService {
  constructor(
    @InjectRepository(Class)
    private classesRepository: Repository<Class>,
  ) {}
  create(createClassDto: CreateClassDto) {
    const classes = this.classesRepository.save(createClassDto);
    if (!classes) {
      throw new NotFoundException();
    }
    return classes;
  }

  findAll() {
    return this.classesRepository.find({
    });
  }

  findOne(id: number) {
    console.log(error)
    return this.classesRepository.findOneBy({ classes_id: id });
  }

  async update(id: number, updateClassDto: UpdateClassDto) {
    const classes = await this.classesRepository.findOneBy({ classes_id: id });
    const updatedClass = { ...classes, ...updateClassDto };
    return this.classesRepository.save(updatedClass);
  }

  async remove(id: number) {
    const classes = await this.classesRepository.findOneBy({ classes_id: id });
    return this.classesRepository.remove(classes);
  }
}
